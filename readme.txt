<================================= DOCUMENTATION ==========>

made by: Mart-Indrek Süld (06.09.2019)
email: mi.syld@gmail.com
goal: Build a JSON over HTTP API endpoint that takes as input two IATA/ICAO airport codes and provides as output a route between these two airports so that the route consists of up to 4 legs/flights (i.e. 3 stops/layovers) and is the shortest such route as measured in kilometers of geographical distance


<========Technologies, libraries and frameworks used ===========>

    spring-boot-starter-web
	lombok
    h2
	spring-boot-starter-data-jpa
	jgrapht-core
    junit

<========== REST APIs ==============>

host: localhost
port: 8080

[GET] - /api/flights/ - Create a flight search request using API-endpoint

<========== REST API parameters =============>

/api/flights/?startAirport=TLL&endAirport=JFK - Search flights from TLL to JFK using IATA-identifiers
/api/flights/?startAirport=EETN&endAirport=KJFK - Search flights from EETN to KFJK using ICAO-identifiers


Syntax:

startAirport={variable}
endAirport={variable}

<==================== H2 Database =======================================>

Data is being presented from static variables
Database is being initialized on the program start from airports.sql and routes.sql files located under resources
Database is being initialized for tests on the program start from test.sql located under resources

<============================== Unit Tests =============================>

Unit tests are performed in the src/test/java/ee/sixfold/flight/FlightApplicationTests.java file

<======================= THE END ========================================>
