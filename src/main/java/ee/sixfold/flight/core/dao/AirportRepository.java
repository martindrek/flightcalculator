package ee.sixfold.flight.core.dao;

import ee.sixfold.flight.core.models.Airport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface AirportRepository extends JpaRepository<Airport, Long> {
    boolean existsByIataIgnoreCase(String iata);
    boolean existsByIcaoIgnoreCase(String icao);

    Optional<Airport> findOneByIataIgnoreCase(String iata);
    Optional<Airport> findOneByIcaoIgnoreCase(String icao);

}
