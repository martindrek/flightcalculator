package ee.sixfold.flight.core.models;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Airport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String airportname;
    private String iata; //3 letter
    private String icao; //4 letter
    private double latitude;
    private double longitude;
}
