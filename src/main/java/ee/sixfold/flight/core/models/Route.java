package ee.sixfold.flight.core.models;

import lombok.Data;
import javax.persistence.*;

@Entity
@Data
public class Route {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String airline;
    private Long airlineId;
    private String sourceIata;
    private Integer sourceId;
    private String destinationIata;
    private Integer destinationId;
}
