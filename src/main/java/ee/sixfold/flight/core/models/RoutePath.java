package ee.sixfold.flight.core.models;

import lombok.Data;
import java.util.List;

@Data
public class RoutePath {
    List<Leg> legs;

    @Data
    public static class Leg {
        private Integer sourceId;
        private String sourceName;
        private Integer destinationId;
        private String destinationName;
    }
}
