package ee.sixfold.flight.core.services.providers.routes;

import ee.sixfold.flight.core.models.RoutePath;

public interface RouteProvider {
    RoutePath getMinFlightPath(Integer originAirport, Integer destinationAirport, int maxPathLength);
}
