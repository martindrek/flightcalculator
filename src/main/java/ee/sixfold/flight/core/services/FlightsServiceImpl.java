package ee.sixfold.flight.core.services;

import ee.sixfold.flight.api.dto.FlightPathRequestDTO;
import ee.sixfold.flight.core.dao.AirportRepository;
import ee.sixfold.flight.core.models.Airport;
import ee.sixfold.flight.core.models.RoutePath;
import ee.sixfold.flight.core.services.providers.location.LocationProvider;
import ee.sixfold.flight.core.services.providers.routes.RouteProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;

@Service
public class FlightsServiceImpl implements FlightsService {
    public static final int IATA_LENGHT = 3;
    public static final int ICAO_LENGHT = 4;

    @Value("${flights.routes.maxlegs}")
    private int maxLegs;

    @Autowired
    RouteProvider flightPathProvider;

    @Autowired
    LocationProvider locationProvider;

    @Autowired
    AirportRepository airportRepository;

    @Override
    public RoutePath findShortestRoute(FlightPathRequestDTO flightDTO) {
        Integer sourceIata = Math.toIntExact(initializeAirport(flightDTO.getStartAirport()).getId());
        Integer destinationIata = Math.toIntExact(initializeAirport(flightDTO.getEndAirport()).getId());

        RoutePath path = flightPathProvider.getMinFlightPath(sourceIata, destinationIata, maxLegs);
        return path;
    }

    private Airport initializeAirport(String airportLabel){
        //length must be between 3 and 4
        if(airportLabel.length() != IATA_LENGHT && airportLabel.length() != ICAO_LENGHT){
            throw new IllegalArgumentException(airportLabel + " search not supported. Length must be between " +IATA_LENGHT+"-"+ICAO_LENGHT);
        }

        //iata or icao must exist in airport
        if(!airportRepository.existsByIataIgnoreCase(airportLabel) && !airportRepository.existsByIcaoIgnoreCase(airportLabel)){
            throw new IllegalArgumentException("Airport " + airportLabel + " not found");
        }

        Airport airport = null;
        if(airportLabel.length() == IATA_LENGHT){
            airport = airportRepository.findOneByIataIgnoreCase(airportLabel).orElseThrow(() -> new EntityNotFoundException(airportLabel + " was not found"));
        } else if(airportLabel.length() == ICAO_LENGHT){
            airport = airportRepository.findOneByIcaoIgnoreCase(airportLabel).orElseThrow(() -> new EntityNotFoundException(airportLabel + " was not found"));
        }

        return airport;
    }
}
