package ee.sixfold.flight.core.services.providers.location.arithmetics;

import ee.sixfold.flight.core.dao.AirportRepository;
import ee.sixfold.flight.core.models.Airport;
import ee.sixfold.flight.core.services.providers.location.LocationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ArithmeticsProvider implements LocationProvider {
    @Autowired
    AirportRepository airportRepository;

    Map<Integer, Airport> airportsMap = new HashMap<>();

    private Airport getAirport(Integer id){
        //if airport hasn't been loaded, load anc cache
        if(!airportsMap.containsKey(id)){
            Airport newAirport = airportRepository.findById(new Long(id)).orElse(null);
            airportsMap.put(id, newAirport);
        }
        return airportsMap.get(id);
    }

    @Override
    public double getDistanceForAirports(Integer airportSourceId, Integer airportDestinationId) {
        Airport start = getAirport(airportSourceId);
        Airport end = getAirport(airportDestinationId);

        //airport is nupp if there was corrupted data found and therefore removed from data source
        if(start == null || end == null){
            return Double.MAX_VALUE;
        }

        return Arithmetics.distance(start.getLatitude(), start.getLongitude(), end.getLatitude(), end.getLongitude(), 'K');
    }

    @Override
    public String getAirportName(Integer source) {
        if(airportsMap.containsKey(source)){
            return airportsMap.get(source).getIata();
        }
        return "N/A";
    }

    //source: https://dzone.com/articles/distance-calculation-using-3
    private static class Arithmetics {
        public static double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
            double theta = lon1 - lon2;
            double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
            dist = Math.acos(dist);
            dist = rad2deg(dist);
            dist = dist * 60 * 1.1515;
            if (unit == 'K') {
                dist = dist * 1.609344;
            } else if (unit == 'N') {
                dist = dist * 0.8684;
            }
            return (dist);
        }

        private static double deg2rad(double deg) {
            return (deg * Math.PI / 180.0);
        }

        private static double rad2deg(double rad) {
            return (rad * 180.0 / Math.PI);
        }
    }
}
