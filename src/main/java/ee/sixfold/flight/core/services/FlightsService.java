package ee.sixfold.flight.core.services;

import ee.sixfold.flight.api.dto.FlightPathRequestDTO;
import ee.sixfold.flight.core.models.RoutePath;

public interface FlightsService {
    RoutePath findShortestRoute(FlightPathRequestDTO flightDTO);
}
