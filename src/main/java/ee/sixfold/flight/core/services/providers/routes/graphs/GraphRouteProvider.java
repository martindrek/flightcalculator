package ee.sixfold.flight.core.services.providers.routes.graphs;

import ee.sixfold.flight.core.dao.RouteRepository;
import ee.sixfold.flight.core.models.Route;
import ee.sixfold.flight.core.services.providers.location.LocationProvider;
import ee.sixfold.flight.core.services.providers.routes.RouteProvider;
import ee.sixfold.flight.core.models.RoutePath;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.AllDirectedPaths;
import org.jgrapht.graph.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import java.util.*;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON) //marked as singleton bean as it's performance expensive to initialize it continuously
class GraphRouteProvider implements RouteProvider {
    DirectedWeightedMultigraph<Integer, DefaultWeightedEdge> graph;
    AllDirectedPaths<Integer, DefaultWeightedEdge> allDirectedPaths;

    LocationProvider locationProvider;

    @Autowired
    public GraphRouteProvider(@Autowired RouteRepository routeRepository, @Autowired LocationProvider locationProvider) {
        this.locationProvider = locationProvider;
        graph = new DirectedWeightedMultigraph<>(DefaultWeightedEdge.class);

        List<Route> routes = routeRepository.findAll();

        for (Route r : routes){
            //as graph's Vertex is Set, only unique elements are added
            graph.addVertex(r.getSourceId());
            graph.addVertex(r.getDestinationId());

            if(!isRouteAddingAllowed(r.getSourceId(), r.getDestinationId())){
                continue;
            }

            graph.setEdgeWeight(graph.addEdge(r.getSourceId(), r.getDestinationId()), locationProvider.getDistanceForAirports(r.getSourceId(), r.getDestinationId()));
        }
        allDirectedPaths = new AllDirectedPaths<>(graph);
    }

    private boolean isRouteAddingAllowed(Integer source, Integer destination){
        //if route already added to graph then forbid
        if(graph.getEdge(source, destination) != null){
            return false;
        }
        //if source and destination are identical then forbid
        if(source.equals(destination)){
            return false;
        }
        return true;
    }

    @Override
    public RoutePath getMinFlightPath(Integer originAirport, Integer destinationAirport, int maxPathLength) {
        List<GraphPath<Integer, DefaultWeightedEdge>> possiblePathList = allDirectedPaths.getAllPaths(originAirport, destinationAirport, false, maxPathLength);
        if(possiblePathList.isEmpty()){
            //no paths found. throw exception?
            return new RoutePath();
        }

        GraphPath<Integer, DefaultWeightedEdge> minPath = possiblePathList.stream().min(Comparator.comparing(GraphPath::getWeight)).get();

        List<RoutePath.Leg> legs = new ArrayList<>();
        for(DefaultWeightedEdge edge : minPath.getEdgeList()){
            RoutePath.Leg leg = new RoutePath.Leg();
            Integer sourceId = graph.getEdgeSource(edge);
            Integer destinationId = graph.getEdgeTarget(edge);
            leg.setSourceId(sourceId);
            leg.setSourceName(locationProvider.getAirportName(sourceId));
            leg.setDestinationId(destinationId);
            leg.setDestinationName(locationProvider.getAirportName(destinationId));
            legs.add(leg);
        }
        RoutePath routePath = new RoutePath();
        routePath.setLegs(legs);
        return routePath;
    }
}
