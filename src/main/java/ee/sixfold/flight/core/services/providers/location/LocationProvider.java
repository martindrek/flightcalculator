package ee.sixfold.flight.core.services.providers.location;

public interface LocationProvider {
    double getDistanceForAirports(Integer airportSourceId, Integer airportDestinationId);
    String getAirportName(Integer source);
}
