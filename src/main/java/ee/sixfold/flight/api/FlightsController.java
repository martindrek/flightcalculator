package ee.sixfold.flight.api;

import ee.sixfold.flight.api.dto.FlightPathRequestDTO;
import ee.sixfold.flight.core.models.RoutePath;
import ee.sixfold.flight.core.services.FlightsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/api/flights")
public class FlightsController {
    @Autowired
    FlightsService flightsService;

    @GetMapping(value = {"/"})
    public ResponseEntity<?> getFlightPath(@ModelAttribute FlightPathRequestDTO flightDTO) {
        RoutePath routePath = flightsService.findShortestRoute(flightDTO);

        return new ResponseEntity<>(routePath, HttpStatus.OK);
    }
}
