package ee.sixfold.flight.api.dto;

import lombok.Data;

@Data
public class FlightPathRequestDTO {
    private String startAirport;
    private String endAirport;
}
