package ee.sixfold.flight;

import ee.sixfold.flight.api.dto.FlightPathRequestDTO;
import ee.sixfold.flight.core.dao.AirportRepository;
import ee.sixfold.flight.core.models.RoutePath;
import ee.sixfold.flight.core.services.FlightsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FlightServiceTests {
    @Autowired
    FlightsService flightsService;

    @Autowired
    AirportRepository airportRepository;

    @Test
    public void whenRouteContainsOverAllowedLegs_thenSkip(){
        FlightPathRequestDTO requestDTO = new FlightPathRequestDTO();
        requestDTO.setStartAirport("TLL");
        requestDTO.setEndAirport("MS3");

        RoutePath routePath = flightsService.findShortestRoute(requestDTO);
        //all defined routes have more legs than allowed
        assertEquals(null, routePath.getLegs());
    }

    @Test
    public void whenMultiplePathFlights_thenReturnShortest(){
        FlightPathRequestDTO requestDTO = new FlightPathRequestDTO();
        requestDTO.setStartAirport("TLL");
        requestDTO.setEndAirport("RIX");

        RoutePath routePath = flightsService.findShortestRoute(requestDTO);
        //JMA is defined as the longest location in test routes data
        for(RoutePath.Leg leg: routePath.getLegs()){
            assertEquals(false, leg.getDestinationName().equals("JMA"));
        }
    }

    //request parameters lenght have to be between 3 and 4
    @Test
    public void whenIllegalParametersUsed_thenDontProcess(){
        System.out.println(airportRepository.count());
        FlightPathRequestDTO requestDTO = new FlightPathRequestDTO();
        requestDTO.setStartAirport("12");
        requestDTO.setEndAirport("34");

        assertThrows(IllegalArgumentException.class, () -> {
            flightsService.findShortestRoute(requestDTO);
        });

        requestDTO.setStartAirport("12345");
        requestDTO.setEndAirport("12345");

        assertThrows(IllegalArgumentException.class, () -> {
            flightsService.findShortestRoute(requestDTO);
        });
    }
}