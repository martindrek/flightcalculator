insert into airport (id, airportname, iata, icao, latitude, longitude) VALUES (1, 'Tallinn', 'TLL', 'EETE', -15.1, -145.1);
insert into airport (id, airportname, iata, icao, latitude, longitude) VALUES (2, 'Riga', 'RIX', 'RIGA', -15.4, -145.4);
insert into airport (id, airportname, iata, icao, latitude, longitude) VALUES (3, 'Moscow', 'MSW', 'MSKA', -15.8, -145.8);
insert into airport (id, airportname, iata, icao, latitude, longitude) VALUES (4, 'Jamaica', 'JMA', 'JMKA', 115.8, 1145.8);
insert into airport (id, airportname, iata, icao, latitude, longitude) VALUES (5, 'Moscow', 'MS1', 'MSK1', -16.8, -146.8);
insert into airport (id, airportname, iata, icao, latitude, longitude) VALUES (6, 'Moscow', 'MS2', 'MSK2', -17.8, -147.8);
insert into airport (id, airportname, iata, icao, latitude, longitude) VALUES (7, 'Moscow', 'MS3', 'MSK3', -18.8, -148.8);

insert into route (airline, airlineId, sourceIata, sourceId, destinationIata, destinationId) VALUES ('E1', 1, 'TLL', 1, 'MSW', 3);
insert into route (airline, airlineId, sourceIata, sourceId, destinationIata, destinationId) VALUES ('E2', 2, 'MSW', 3, 'RIX', 2);

insert into route (airline, airlineId, sourceIata, sourceId, destinationIata, destinationId) VALUES ('A1', 3, 'TLL', 1, 'JMA', 4);
insert into route (airline, airlineId, sourceIata, sourceId, destinationIata, destinationId) VALUES ('A2', 4, 'JMA', 4, 'MSW', 3);
insert into route (airline, airlineId, sourceIata, sourceId, destinationIata, destinationId) VALUES ('A3', 5, 'MSW', 3, 'RIX', 2);

insert into route (airline, airlineId, sourceIata, sourceId, destinationIata, destinationId) VALUES ('A5', 6, 'TLL', 1, 'RIX', 2);
insert into route (airline, airlineId, sourceIata, sourceId, destinationIata, destinationId) VALUES ('A6', 7, 'RIX', 2, 'MSW', 3);
insert into route (airline, airlineId, sourceIata, sourceId, destinationIata, destinationId) VALUES ('A7', 8, 'MSW', 3, 'MS1', 5);
insert into route (airline, airlineId, sourceIata, sourceId, destinationIata, destinationId) VALUES ('A8', 9, 'MS1', 5, 'MS2', 6);
insert into route (airline, airlineId, sourceIata, sourceId, destinationIata, destinationId) VALUES ('A9', 10, 'MS2', 6, 'MS3', 7);


